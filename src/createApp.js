import express from "express"
import { initializeFfManager } from "./featureFlag.js"
import { tagRecipeDifficulty } from "./difficultyTagger.js"
import { DbRecipe } from "./DbRecipe.js"
const isEnabled = await initializeFfManager()

// let DbRecipe = null

// export function setDbRecipe(_DbRecipe){
//     DbRecipe = _DbRecipe
// }
export async function createApp() {
    const app = express()
    app.use(express.urlencoded({ extended: true }))

    app.set("view engine", "ejs")
    app.set("views", "./src/views")

    app.get("/home", async (req, res) => {
        let recipes = await DbRecipe.find()
        recipes = recipes.map((recipe) => ({
            _id:recipe._id,
            name:recipe.name,
            difficulty: tagRecipeDifficulty(recipe)
        }))
        const showLoginButton = await isEnabled('login_button')
        res.render("home", { recipes , showLoginButton })
    })
    app.post("/recipe", async (req, res) => {
        const recipe = req.body
        const dbRecipe = new DbRecipe(recipe)
        await dbRecipe.save()
        res.redirect("/home")
    })
    return app
}